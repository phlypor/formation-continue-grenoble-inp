{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77228f5b",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "use CTRL+ENTER or SHIFT+ENTER\n",
    "with the cursor placed in this cell\n",
    "and observe\n",
    "\"\"\"\n",
    "try:\n",
    "    allegro\n",
    "    from importlib import reload\n",
    "    reload(allegro)\n",
    "except NameError:\n",
    "    import allegro"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8dd5c479",
   "metadata": {},
   "source": [
    "# 2. Regression\n",
    "\n",
    "## The problem\n",
    "\n",
    "Given a collection of samples with their **target**, i.e., tuples $(x_i,y_i)$ consisting of a sample $x_i$ with its corresponding target $y_i$, learn a model $g\\in\\mathcal G$ such that $d(y_i, g(x_i))$ is on average minimised.\n",
    "\n",
    "## The plan\n",
    "\n",
    "* decide upon a model family $\\mathcal G$, rich enough to model the data, but not richer (cf. Occam's rasor), often the datatypes (ordinal, numerical, categorical) restrain your choices.\n",
    "* decide upon a distance (or divergence) measure $d(\\cdot,\\cdot)$ that will yield a penalty associated with $y_i\\neq g(x_i)$\n",
    "* partition your dataset into \n",
    "    * **training** data: to fit the model (interpolation)\n",
    "    * **validation** data: for tuning hyperparameters in the model family, also allows for early stopping when generaliation capabilities of the model is put to danger\n",
    "    * **testing** data: to (sanity) check the performance of the final model\n",
    "* learn the model on a **training** set by minimising the penalty, observe how the penalty evolves on the **validation** set\n",
    "* sanity check your model on the **test** set\n",
    "\n",
    "<div class=\"alert alert-danger\">\n",
    "    Often times in machine learning the roles of <b>validation</b> and <b>test</b> set are permuted (Ripley, Brian D. 2009. Pattern recognition and neural networks. Cambridge Univ. Press.). We will restrain ourselves to the above nomenclature.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6abf2fb2",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\">\n",
    "    Categorical data is not supported in scikit-learn, but one may convert categorical data into numerical data using a one-hot encoder. A one-hot encoder returns a canonical vector for the $i$th category as\n",
    "    $$f(\\mathcal C_i) = (\\underbrace{0, 0, \\ldots, 0}_{\\times(i-1)},1, \\underbrace{0, 0, \\ldots, 0}_{\\times(k-i)})$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45e391e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "# example of categorical encoding with one-hot encoder\n",
    "from sklearn.preprocessing import OneHotEncoder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab8d6491",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('data/AirBnBLyon.csv')\n",
    "set(df['room_type'])  # the different room types"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "306dadc9",
   "metadata": {},
   "source": [
    "### Variables in regression\n",
    "\n",
    "The dataset AirBnB has features that are either categorical (`room_type`,, ...) or numerical (`longitude`, `latitude`, ...). Only few regressors are suited to deal with both at the same time, the decision trees are one of these.\n",
    "\n",
    "Among the different variable types that exist we find\n",
    "\n",
    "|data type| description|example|\n",
    "|--|:--|:--:|\n",
    "|numerical| Data can be ordered and allows for a distance measure (and hence a triangular inequality). Numerical data can be of discrete or continuous nature.|real numbers|\n",
    "|ordinal| Data can be ordered, but there is no distance measure. The order is transitive, but it is impossible to compare the _difference_ between the first and second to the difference between the fifth and the sixth, for instance.|preference ranking|\n",
    "|categorical| Data can be labelled, but there is no ordering, i.e., all labels are treated equally.|taxonomy|\n",
    "\n",
    "Apart from the data type, the data plays different roles.\n",
    "\n",
    "|role|description|\n",
    "|--|:--|\n",
    "|feature| The attributes of the data, often denoted by a feature vector $x_i$.|\n",
    "|target| A value associated with the current sample, it is supposed that there exists some relationship $y_i\\approx \\phi(x_i)$ for all $i$. In regression the target value is numerical.|\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "076f3fbc",
   "metadata": {},
   "source": [
    "## 2.1 Variable preprocessing\n",
    "\n",
    "Unfortunately, scikit-learn does not admit categorical variables, we will thus use the one-hot encoder to mimic a categorical variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1221e69",
   "metadata": {},
   "outputs": [],
   "source": [
    "# example of categorical encoding with one-hot encoder\n",
    "from sklearn.preprocessing import OneHotEncoder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2028de7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "ohe = OneHotEncoder()\n",
    "# make sure there is one line per sample\n",
    "room = [[x] for x in df['room_type']] \n",
    "ohe.fit(room)\n",
    "\n",
    "for c in ohe.categories_[0]:\n",
    "    l = len(c)\n",
    "    print('{category:<20s} -->\\t {encoding}'.format(\n",
    "        category=c,\n",
    "        encoding=ohe.transform([[c]]).toarray()))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3c92df5",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\"><b>Redundancy.</b> The as such constructed feature has some redundancy, can you see why?</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "652b36e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "ohe.transform(room).toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56208a04",
   "metadata": {},
   "outputs": [],
   "source": [
    "# of course, we can revert back to the category names\n",
    "ohe.inverse_transform(ohe.transform(room))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b97c7d0b",
   "metadata": {},
   "source": [
    "## 2.2 Training, validation, and testing data\n",
    "\n",
    "In scikit-learn, splitting data into training, validation, and testing can be done in [many different ways](https://scikit-learn.org/stable/modules/cross_validation.html#cross-validation-iterators).\n",
    "\n",
    "|split|description|\n",
    "|--|:--|\n",
    "| `LeaveOneOut`| leave one sample aside for testing, the others for training ($n$ possibilities that can be iterated), can be extended to `LeavePOut` ($\\binom{n}{p}$ possibilities; test sets are not complementary)|\n",
    "|`KFold`| split data in $K$ equally sized partitions and perform a leave-one-fold-out|\n",
    "|`ShuffleSplit`| randomly shuffle data and divide in train and test set (using a percentage of data as test data), the number of independent \"datasets\" must be specified|\n",
    "\n",
    "The latter is closest to statistical _jackknifing_ (random sampling without replacement), an alternative would be _bootstrapping_ (random sampling with replacement)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4566f618",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn import model_selection as ms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c69c1256",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "ss = ms.ShuffleSplit(n_splits=1, test_size=0.33)\n",
    "for train_ix, test_ix in ss.split(df):\n",
    "    display(df.loc[train_ix])\n",
    "    display(df.loc[test_ix])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efddec4d",
   "metadata": {},
   "source": [
    "## 2.3 Scoring goodness-of-fit\n",
    "\n",
    "The scoring is often based on an $R^2$-like value, defined as\n",
    "$$\n",
    "\\text{score}(X,y) = 1 - \\frac{\\text{unexplained variance}}{\\text{total variance}} \\in]-\\infty, 1]\n",
    "$$\n",
    "The higher the score, the better. When negative, the regressor actually introduces supplementary variance.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "For a linear regression under mean-squared error, we find the optimal variables $(a,b)$ that minimise $\\|y_i - (a x_i + b)\\|^2$ and call them $(a_{\\text{opt}}, b_{\\text{opt}})$. The mean-squared error on the <b>training set</b> is then given by\n",
    "$$\n",
    "\\begin{align}\n",
    "\\frac{1}{n_{\\text{train}}}\\sum_{i\\in\\text{train}}\\|y_i - (a_{\\text{opt}} x_i + b_{\\text{opt}})\\|^2 &= \\mathrm{var}(y)\\left(1-R^2\\right) \\\\&= \\text{unexplained variance}\\end{align}\n",
    "$$\n",
    "where $\\mathrm{var}$ denotes the empirical variance (denoted as total variance) and $R$ is Pearson's correlation coefficient. Hence, we obtain\n",
    "$$\n",
    "\\text{score}(X,y) = R^2 \\in[0, 1]\n",
    "$$\n",
    "If $y$ can be perfectly predicted from $X$, then we must have that the explained variance is the total variance, hence $R^2=1$ and the score is maximal ($=1$).\n",
    "</div>\n",
    "\n",
    "\n",
    "## 2.4 Cross-validation\n",
    "\n",
    "A result may be due to an unlucky choice of the data split. In order to estimate the variability and mean value of the score, we may carry out a cross-validation. The goal is to test on multiple train--test splits and estimate the distribution of the score on out-of-sample (testing; data not seen during training) data. Most often K-fold cross-validation is encountered.\n",
    "\n",
    "Cross-validation can be used to determine the hyper-parameter that seems best suited for the regression task at hand, refraining from overfitting.\n",
    "\n",
    "Examples will be given below."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0e251a1",
   "metadata": {},
   "source": [
    "## 2.5 Some specific examples\n",
    "\n",
    "Throughout, we will use two datasets for which we introduce the following prediction tasks:\n",
    "\n",
    "* predict rental pricing: using numerical and categorical features to predict a (continuous) numerical value\n",
    "* predict wine quality: using numerical features to predict a discrete numerical value\n",
    "\n",
    "\n",
    "1. [Nearest neighbour regression](notebooks/02_Regression/NN.ipynb)\n",
    "2. [Decision Tree Regression](notebooks/02_Regression/DecisionTrees.ipynb)\n",
    "3. [Artificial Neural Network](notebooks/02_Regression/ANN.ipynb)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
