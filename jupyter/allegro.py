from IPython.display import HTML, Image, display

# some fancy initialisation to get the logos at the beginning of the files
# it also comes with a specific way of unfolding div elements

# solution inspired by https://www.digitalocean.com/community/tutorials/css-collapsible and https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details
h = HTML('''
<style type="text/css">
details {
    background: #FAFAFA;
    border-bottom: 1px solid rgba(250, 224, 66, .45);
    border-bottom-left-radius: 7px;
    border-bottom-right-radius: 7px;
    padding: .5rem 1rem;
}

summary {
    display: block;

    font-weight: bold;
    font-family: monospace;
    font-size: 1.2rem;
    text-transform: uppercase;
    text-align: center;

    padding: 1rem;

    color: #A77B0E;
    background: #EAEAEA;

    cursor: pointer;

    border-radius: 7px;
}

details[open] {
    padding: .5em;
}

details[open] summary {
    border-bottom: 1px solid #aaa;
    margin-bottom: .5em;
}

.author {
  text-align: right;
  font-style: italic;
}

.author::before {
  content: '( ';
  font-style: normal;
}

.author::after {
  content:' )';
  font-style: normal;
}
</style>
'''
)

display(h)
display(Image(filename='./images/logo.png'))
