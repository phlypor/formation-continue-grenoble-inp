{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6d4562dd",
   "metadata": {},
   "source": [
    "# Linear dimension reduction\n",
    "\n",
    "Suppose we have the following training set of features $x\\in\\mathbb R^{n\\times k}$, where $n$ is the number of samples and $k$ the number of features."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2cd9a214",
   "metadata": {},
   "source": [
    "## Unsupervised\n",
    "\n",
    "### The plan\n",
    "\n",
    "Keep as much of the information as possible while reducing the dimension, where information is determined to be the variance.\n",
    "\n",
    "The objective can be transcribed as:\n",
    "Given $d<k$, find $(a, b)$ with $a\\in\\mathbb R^{n\\times d}$ and $b\\in\\mathbb R^{d\\times k}$ such that\n",
    "$$\n",
    "\\|x - ab\\|_F^2\n",
    "$$\n",
    "is minimal. (Alternatively, one may look for the minimal $d$ such that $\\|x-ab\\|_F^2\\leq \\varepsilon$).\n",
    "\n",
    "Since $\\|x\\|_F^2$ is constant, one may rewrite this as \n",
    "$$\n",
    "\\frac{\\|x-ab\\|_F^2}{\\|x\\|_F^2} = 1-R^2\n",
    "$$\n",
    "which is the unexplained variance divide by the total variance, with $x$ being its own target!\n",
    "\n",
    "Remark that the solution is not unique, and one imposes the supplementary constraints that $ab=u\\sigma v^t$ where $u^tu = \\mathrm{Id}$, $v^tv=\\mathrm{Id}$, and $\\sigma$ is a diagonal matrix with positive, decreasing elements along its diagonal. Hence, we may rewrite $uu^t(x - ab)vv^t = uu^t(x - u\\sigma v^t)vv^t = uu^txvv^t - u\\sigma v^t$, from which we observe, that $ab$ is the bilinear orthogonal projection of $x$. \n",
    "\n",
    "$v$ are the principal components, i.e., for a given sample $x_i$, the transformed feature vector is given by $x_iv$. if we use zero-mean $x_{\\text{train}}$, then we have that the transformed features $xv\\sigma^{-1}$ are of unit variance!\n",
    "\n",
    "<div class=\"alert alert-info\">The difference with feature selection seems only superficial, though it is of big importance that one must feed all features to the transformer. If features are costly to obtain (computationally or economically), feature selection might be a better approach. In addition, transformed features are often more difficult to attach a physical meaning to.</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a55dee11",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "import sklearn.preprocessing as preproc\n",
    "import sklearn.model_selection as modsel\n",
    "from sklearn.decomposition import PCA\n",
    "from sklearn.neural_network import MLPClassifier\n",
    "\n",
    "%matplotlib notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f64feef2",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('../../data/leafs/features.txt', 'r') as f:\n",
    "    features = [x.rstrip('\\n') for x in f]  # .txt file constructed with info from pdf\n",
    "    if not features[-1]: features.pop()  # if last line only contained '\\n', ignore this\n",
    "        \n",
    "leafs = pd.read_csv('../../data/leafs/leaf.csv', header=None, names=features,index_col=False)\n",
    "leafs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10deb097",
   "metadata": {},
   "outputs": [],
   "source": [
    "features = set(leafs.columns) - set(['class'])\n",
    "classes = 'class'\n",
    "X = leafs[features]\n",
    "y = leafs[classes]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ab30569",
   "metadata": {},
   "outputs": [],
   "source": [
    "strat = modsel.StratifiedShuffleSplit(n_splits=1, test_size=.2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "370f6803",
   "metadata": {},
   "outputs": [],
   "source": [
    "for train_ix, test_ix in strat.split(X, y):\n",
    "    X_train, y_train = X.loc[train_ix], y.loc[train_ix]\n",
    "    X_test, y_test = X.loc[test_ix], y.loc[test_ix]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c1ce11b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Only fit onto the training data !!\n",
    "pca = PCA(n_components=2)\n",
    "X_train_ = pca.fit_transform(X_train)\n",
    "X_test_ = pca.transform(X_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f1b19ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "ann = MLPClassifier(hidden_layer_sizes=(100,), max_iter=10000)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3847cf3",
   "metadata": {},
   "outputs": [],
   "source": [
    "ann.fit(X_train_, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3d2375d",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_loss, ax_loss = plt.subplots()\n",
    "ax_loss.plot(ann.loss_curve_)\n",
    "ax_loss.set_xlabel('iteration index')\n",
    "ax_loss.set_ylabel('cross-entropy')\n",
    "ax_loss.set_title('cross-entropy evolution during training')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aca00c3c",
   "metadata": {},
   "outputs": [],
   "source": [
    "y_pred = ann.predict(X_test_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e726fed9",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_pred, ax_pred = plt.subplots()\n",
    "ax_pred.scatter(y_test, y_pred)\n",
    "ax_pred.set_xlabel('true class label')\n",
    "ax_pred.set_ylabel('predicted class label')\n",
    "ax_pred.set_title('ann: mean accuracy={:.3}'.format(ann.score(X_test_, y_test)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59982a35",
   "metadata": {},
   "outputs": [],
   "source": [
    "probas = ann.predict_proba(X_test_)\n",
    "ohe = preproc.OneHotEncoder(sparse=False)\n",
    "ohe.fit(y_test[:, None])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ccb446f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_prob, ax_prob = plt.subplots(ncols=2)\n",
    "ax_prob[1].matshow(probas)\n",
    "ax_prob[1].set_xlabel('class label')\n",
    "ax_prob[1].set_ylabel('test sample index')\n",
    "ax_prob[1].set_title('posterior probabilities')\n",
    "\n",
    "ax_prob[0].matshow(ohe.transform(y_test[:, None]))\n",
    "ax_prob[0].set_xlabel('class label')\n",
    "ax_prob[0].set_ylabel('test sample index')\n",
    "ax_prob[0].set_title('true labels')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "809a7ba7",
   "metadata": {},
   "source": [
    "### Exercice\n",
    "\n",
    "We observe that 2 features is far too few to be able to classify the dataset properly.\n",
    "* Create a [pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html#sklearn.pipeline.Pipeline) that chains the PCA preprocessing step with the ANN classifier\n",
    "* Do a cross-validation on the number of PCA components `n_components`, keeping the dimension of the network fix"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "816fca09",
   "metadata": {},
   "source": [
    "## Supervised\n",
    "\n",
    "We will not treat this here explicitely, but you may have a look at\n",
    "\n",
    "* [Partial Least Squares](https://scikit-learn.org/stable/modules/classes.html#module-sklearn.cross_decomposition): PCA for regression problems, transforming both features and targets\n",
    "* [Lasso](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html?highlight=lasso#sklearn.linear_model.Lasso): linear regression with simultaneous feature selection"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
