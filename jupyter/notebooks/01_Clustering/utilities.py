# utility function for plotting
import numpy as np
from scipy.cluster.hierarchy import dendrogram
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap

colors = get_cmap('tab20').colors


def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_,
                                      counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)

def plot_spatial(model, df, **kwargs):
    fig, ax = plt.subplots()

    for ii, lb in enumerate(set(model.labels_)):
        df[model.labels_==lb].plot.scatter(
            y='latitude',
            x='longitude',
            ax=ax,
            label='$\\mathcal{{C}}_{{{}}}$'.format(lb+1),
            color=colors[ii],
            s=4
        )
    ax.legend(loc='upper right')
    return fig, ax