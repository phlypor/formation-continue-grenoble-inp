# The origins of the data in this folder

|data|origin|
|--|--|
|AirBnBLyon.csv|[insideairbnb](http://insideairbnb.com/get-the-data.html)|
|winequality-*.csv|[UCI datasets](https://archive.ics.uci.edu/ml/datasets/wine+quality)|
|leafs/|[Leaf Data Set](https://archive.ics.uci.edu/ml/datasets/Leaf)|
|![drop.jpg](./drop.jpg)|[by Pablo Perez from FreeImages](https://www.freeimages.com/fr/photo/multicolor-drop-2-1056473)|
|![umbrella](./umbrella.jpg)|[by Neke Moor from FreeImages](https://www.freeimages.com/fr/photo/rainbow-umbrella-2-1190681)|
|![rainbow](./rainbow.jpg)|[by Cheryl Empey from FreeImages](https://www.freeimages.com/fr/photo/rainbow-1369910)|
