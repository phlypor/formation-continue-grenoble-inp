# The origins of the data in this folder

All of the data that is not explicitely mentionned in the below table is our own and can be freely redistributed under the [creative commons CC BY-SA 4.0](licence.md).

|data|origin|
|--|--|
| ![logo.png](logo.png)| copyright owned by Grenoble INP Formation Continue &amp; Allegro|
| ![ann_layers.svg](ann_layers.svg)| CC BY-SA 3.0 by [Glosser.ca](https://commons.wikimedia.org/wiki/User_talk:Glosser.ca) as found on [wikipedia](https://en.wikipedia.org/wiki/Artificial_neural_network) |
| ![neurons.png](neurons.png) | CC BY-SA 4.0 by [Egm4313.s12](https://commons.wikimedia.org/wiki/User:Egm4313.s12) as found on [wikipedia](https://en.wikipedia.org/wiki/Artificial_neural_network) |